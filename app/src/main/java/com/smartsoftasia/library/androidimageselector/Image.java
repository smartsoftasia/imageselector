package com.smartsoftasia.library.androidimageselector;

import android.net.Uri;

/**
 * Created by ssa-dev-4 on 19/5/2560.
 */

public class Image {
  public Uri uri;

  public Image(Uri uri) {
    this.uri = uri;
  }
}
