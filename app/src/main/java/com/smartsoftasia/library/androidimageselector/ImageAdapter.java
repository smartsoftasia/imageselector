package com.smartsoftasia.library.androidimageselector;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

/**
 * Created by ssa-dev-4 on 19/5/2560.
 */

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {

  private Context context;
  private List<Image> images;

  public class ViewHolder extends RecyclerView.ViewHolder {
    private ImageView image;

    public ViewHolder(View view) {
      super(view);
      image = (ImageView) view.findViewById(R.id.imageView);
    }
  }

  public ImageAdapter(Context context, List<Image> images) {
    this.context = context;
    this.images = images;
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View itemView = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.adapter_image, parent, false);
    return new ViewHolder(itemView);
  }

  @Override
  public void onBindViewHolder(ViewHolder holder, int position) {
    Image image = images.get(position);
    holder.image.setImageURI(image.uri);
  }

  @Override
  public int getItemCount() {
    return images.size();
  }

  public void appendNewsItems(List<Image> images) {
    this.images.clear();
    this.images.addAll(images);
    this.notifyDataSetChanged();
  }
}