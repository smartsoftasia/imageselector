package com.smartsoftasia.library.androidimageselector;

import android.net.Uri;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.smartsoftasia.library.imageselector.*;
import com.smartsoftasia.library.imageselector.BuildConfig;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import rx.functions.Action1;

public class MainActivity extends AppCompatActivity {

  private Button cameraButton;
  private Button galleryButton;
  private RecyclerView recyclerView;
  private ImageAdapter imageAdapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    cameraButton = (Button) findViewById(R.id.button_camera);
    cameraButton.setOnClickListener(
        new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            takeCamera();
          }
        }
    );

    galleryButton = (Button) findViewById(R.id.button_gallery);
    galleryButton.setOnClickListener(
        new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            takeGallery();
          }
        }
    );

    imageAdapter = new ImageAdapter(this, new ArrayList<Image>());
    recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
    recyclerView.setLayoutManager(new LinearLayoutManager(this));
    recyclerView.setAdapter(imageAdapter);
  }

  private void takeCamera() {
    RxImagePicker.with(this).requestCamera().subscribe(
        new Action1<Uri>() {
          @Override
          public void call(Uri uri) {
            List<Image> images = new ArrayList<>();
            images.add(new Image(uri));
            imageAdapter.appendNewsItems(images);
          }
        });
  }

  private void takeGallery() {
    RxImagePicker.with(this).requestGallery().subscribe(new Action1<List<Uri>>() {
      @Override
      public void call(List<Uri> uris) {
        List<Image> images = new ArrayList<>();
        for (int i = 0; i < uris.size(); i++) {
          images.add(new Image(uris.get(i)));
        }
        imageAdapter.appendNewsItems(images);
      }
    });

  }

}