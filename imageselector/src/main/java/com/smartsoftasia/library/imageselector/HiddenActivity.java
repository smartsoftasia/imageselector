package com.smartsoftasia.library.imageselector;

import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.media.ExifInterface;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by ssa-dev-4 on 16/5/2560.
 */
public class HiddenActivity extends Activity {

  public static final String ARG_SOURCE = "arg_source";
  public static final int SOURCE_CAMERA = 0;
  public static final int SOURCE_GALLERY = 1;

  private static final String KEY_CAMERA_URI = "key_camera_uri";
  private static final int SELECT_CAMERA = 100;
  private static final int SELECT_GALLERY = 101;

  private Uri cameraUri;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    startTakeImageIntent();
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    outState.putParcelable(KEY_CAMERA_URI, cameraUri);
    super.onSaveInstanceState(outState);
  }

  @Override
  protected void onRestoreInstanceState(Bundle savedInstanceState) {
    super.onRestoreInstanceState(savedInstanceState);
    cameraUri = savedInstanceState.getParcelable(KEY_CAMERA_URI);
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                         int[] grantResults) {
    if (grantResults.length > 0
        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
      startTakeImageIntent();
    } else {
      finish();
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (resultCode == RESULT_OK) {
      switch (requestCode) {
        case SELECT_GALLERY:
          handleGalleryResult(data);
          break;
        case SELECT_CAMERA:
          // test(cameraUri.toString());
          RxImagePicker.with(this).onImagePicked(cameraUri);
          break;
      }
    }
    finish();
  }

  private void startTakeImageIntent() {
    switch (getArgSource()) {
      case SOURCE_CAMERA:
        if (!checkCameraPermission()) {
          return;
        }
        cameraUri = createCameraUri();
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, cameraUri);

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
          cameraIntent.setClipData(ClipData.newRawUri("", cameraUri));
          cameraIntent.addFlags(
              Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        startActivityForResult(cameraIntent, SELECT_CAMERA);
        break;
      case SOURCE_GALLERY:
        if (!checkGalleryPermission()) {
          return;
        }

        Intent galleryIntent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
          galleryIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
          galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
          galleryIntent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
        } else {
          galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        }
        galleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        galleryIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, SELECT_GALLERY);
        break;
    }
  }

  private void handleGalleryResult(Intent data) {
    ArrayList<Uri> uris = new ArrayList<>();
    if (data.getClipData() != null) {
      // Multiple images
      ClipData clipData = data.getClipData();
      for (int i = 0; i < clipData.getItemCount(); i++) {
        uris.add(deleteMetaData(clipData.getItemAt(i).getUri()));
      }
    } else {
      // One image
      uris.add(deleteMetaData(data.getData()));
    }
    RxImagePicker.with(this).onImagesPicked(uris);
  }

  private boolean checkCameraPermission() {
    if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) !=
        PackageManager.PERMISSION_GRANTED) {
      ActivityCompat.requestPermissions(this,
          new String[]{Manifest.permission.CAMERA}, 0);
      return false;
    } else {
      return true;
    }
  }

  private boolean checkGalleryPermission() {
    if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) !=
        PackageManager.PERMISSION_GRANTED) {
      ActivityCompat.requestPermissions(this,
          new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
      return false;
    } else {
      return true;
    }
  }

  private Uri createCameraUri() {
    try {
      File file = File.createTempFile("image_", ".jpg", getCacheDir());
      file.setWritable(true, true);

      return FileProvider
          .getUriForFile(HiddenActivity.this,
              getPackageName() + ".fileprovider", file);
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    }
  }

  private Uri deleteMetaData(Uri uri) {
    try {
      String path;
      if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
        path = RealPathHelper.getRealPathFromURI_API11to18(this, uri);
      } else {
        path = RealPathHelper.getRealPathFromURI_API19(this, uri);
      }

      File sourceFile = new File(path);
      File destinationFile = File.createTempFile("image_", ".jpg", getCacheDir());
      copyFile(sourceFile, destinationFile);

      ExifInterface exifInterface = new ExifInterface(destinationFile.getAbsolutePath());
      exifInterface.setAttribute(ExifInterface.TAG_ARTIST, null);
      exifInterface.setAttribute(ExifInterface.TAG_BITS_PER_SAMPLE, null);
      exifInterface.setAttribute(ExifInterface.TAG_COMPRESSION, null);
      exifInterface.setAttribute(ExifInterface.TAG_COPYRIGHT, null);
      exifInterface.setAttribute(ExifInterface.TAG_DATETIME, null);
      exifInterface.setAttribute(ExifInterface.TAG_IMAGE_DESCRIPTION, null);
      exifInterface.setAttribute(ExifInterface.TAG_IMAGE_LENGTH, null);
      exifInterface.setAttribute(ExifInterface.TAG_IMAGE_WIDTH, null);
      exifInterface.setAttribute(ExifInterface.TAG_JPEG_INTERCHANGE_FORMAT, null);
      exifInterface.setAttribute(ExifInterface.TAG_JPEG_INTERCHANGE_FORMAT_LENGTH, null);
      exifInterface.setAttribute(ExifInterface.TAG_MAKE, null);
      exifInterface.setAttribute(ExifInterface.TAG_MODEL, null);
      exifInterface.setAttribute(ExifInterface.TAG_ORIENTATION, null);
      exifInterface.setAttribute(ExifInterface.TAG_PHOTOMETRIC_INTERPRETATION, null);
      exifInterface.setAttribute(ExifInterface.TAG_PLANAR_CONFIGURATION, null);
      exifInterface.setAttribute(ExifInterface.TAG_PRIMARY_CHROMATICITIES, null);
      exifInterface.setAttribute(ExifInterface.TAG_REFERENCE_BLACK_WHITE, null);
      exifInterface.setAttribute(ExifInterface.TAG_RESOLUTION_UNIT, null);
      exifInterface.setAttribute(ExifInterface.TAG_ROWS_PER_STRIP, null);
      exifInterface.setAttribute(ExifInterface.TAG_SAMPLES_PER_PIXEL, null);
      exifInterface.setAttribute(ExifInterface.TAG_SOFTWARE, null);
      exifInterface.setAttribute(ExifInterface.TAG_STRIP_BYTE_COUNTS, null);
      exifInterface.setAttribute(ExifInterface.TAG_STRIP_OFFSETS, null);
      exifInterface.setAttribute(ExifInterface.TAG_TRANSFER_FUNCTION, null);
      exifInterface.setAttribute(ExifInterface.TAG_WHITE_POINT, null);
      exifInterface.setAttribute(ExifInterface.TAG_X_RESOLUTION, null);
      exifInterface.setAttribute(ExifInterface.TAG_Y_CB_CR_COEFFICIENTS, null);
      exifInterface.setAttribute(ExifInterface.TAG_Y_CB_CR_POSITIONING, null);
      exifInterface.setAttribute(ExifInterface.TAG_Y_CB_CR_SUB_SAMPLING, null);
      exifInterface.setAttribute(ExifInterface.TAG_Y_RESOLUTION, null);
      exifInterface.setAttribute(ExifInterface.TAG_APERTURE_VALUE, null);
      exifInterface.setAttribute(ExifInterface.TAG_BRIGHTNESS_VALUE, null);
      exifInterface.setAttribute(ExifInterface.TAG_CFA_PATTERN, null);
      exifInterface.setAttribute(ExifInterface.TAG_COLOR_SPACE, null);
      exifInterface.setAttribute(ExifInterface.TAG_COMPONENTS_CONFIGURATION, null);
      exifInterface.setAttribute(ExifInterface.TAG_COMPRESSED_BITS_PER_PIXEL, null);
      exifInterface.setAttribute(ExifInterface.TAG_CONTRAST, null);
      exifInterface.setAttribute(ExifInterface.TAG_CUSTOM_RENDERED, null);
      exifInterface.setAttribute(ExifInterface.TAG_DATETIME_DIGITIZED, null);
      exifInterface.setAttribute(ExifInterface.TAG_DATETIME_ORIGINAL, null);
      exifInterface.setAttribute(ExifInterface.TAG_DEVICE_SETTING_DESCRIPTION, null);
      exifInterface.setAttribute(ExifInterface.TAG_DIGITAL_ZOOM_RATIO, null);
      exifInterface.setAttribute(ExifInterface.TAG_EXIF_VERSION, null);
      exifInterface.setAttribute(ExifInterface.TAG_EXPOSURE_BIAS_VALUE, null);
      exifInterface.setAttribute(ExifInterface.TAG_EXPOSURE_INDEX, null);
      exifInterface.setAttribute(ExifInterface.TAG_EXPOSURE_MODE, null);
      exifInterface.setAttribute(ExifInterface.TAG_EXPOSURE_PROGRAM, null);
      exifInterface.setAttribute(ExifInterface.TAG_EXPOSURE_TIME, null);
      exifInterface.setAttribute(ExifInterface.TAG_F_NUMBER, null);
      exifInterface.setAttribute(ExifInterface.TAG_FILE_SOURCE, null);
      exifInterface.setAttribute(ExifInterface.TAG_FLASH, null);
      exifInterface.setAttribute(ExifInterface.TAG_FLASH_ENERGY, null);
      exifInterface.setAttribute(ExifInterface.TAG_FLASHPIX_VERSION, null);
      exifInterface.setAttribute(ExifInterface.TAG_FOCAL_LENGTH, null);
      exifInterface.setAttribute(ExifInterface.TAG_FOCAL_LENGTH_IN_35MM_FILM, null);
      exifInterface.setAttribute(ExifInterface.TAG_FOCAL_PLANE_RESOLUTION_UNIT, null);
      exifInterface.setAttribute(ExifInterface.TAG_FOCAL_PLANE_X_RESOLUTION, null);
      exifInterface.setAttribute(ExifInterface.TAG_FOCAL_PLANE_Y_RESOLUTION, null);
      exifInterface.setAttribute(ExifInterface.TAG_GAIN_CONTROL, null);
      exifInterface.setAttribute(ExifInterface.TAG_ISO_SPEED_RATINGS, null);
      exifInterface.setAttribute(ExifInterface.TAG_IMAGE_UNIQUE_ID, null);
      exifInterface.setAttribute(ExifInterface.TAG_LIGHT_SOURCE, null);
      exifInterface.setAttribute(ExifInterface.TAG_MAKER_NOTE, null);
      exifInterface.setAttribute(ExifInterface.TAG_MAX_APERTURE_VALUE, null);
      exifInterface.setAttribute(ExifInterface.TAG_METERING_MODE, null);
      exifInterface.setAttribute(ExifInterface.TAG_OECF, null);
      exifInterface.setAttribute(ExifInterface.TAG_PIXEL_X_DIMENSION, null);
      exifInterface.setAttribute(ExifInterface.TAG_PIXEL_Y_DIMENSION, null);
      exifInterface.setAttribute(ExifInterface.TAG_RELATED_SOUND_FILE, null);
      exifInterface.setAttribute(ExifInterface.TAG_SATURATION, null);
      exifInterface.setAttribute(ExifInterface.TAG_SCENE_CAPTURE_TYPE, null);
      exifInterface.setAttribute(ExifInterface.TAG_SCENE_TYPE, null);
      exifInterface.setAttribute(ExifInterface.TAG_SENSING_METHOD, null);
      exifInterface.setAttribute(ExifInterface.TAG_SHARPNESS, null);
      exifInterface.setAttribute(ExifInterface.TAG_SHUTTER_SPEED_VALUE, null);
      exifInterface.setAttribute(ExifInterface.TAG_SPATIAL_FREQUENCY_RESPONSE, null);
      exifInterface.setAttribute(ExifInterface.TAG_SPECTRAL_SENSITIVITY, null);
      exifInterface.setAttribute(ExifInterface.TAG_SUBSEC_TIME, null);
      exifInterface.setAttribute(ExifInterface.TAG_SUBSEC_TIME_DIGITIZED, null);
      exifInterface.setAttribute(ExifInterface.TAG_SUBSEC_TIME_ORIGINAL, null);
      exifInterface.setAttribute(ExifInterface.TAG_SUBJECT_AREA, null);
      exifInterface.setAttribute(ExifInterface.TAG_SUBJECT_DISTANCE, null);
      exifInterface.setAttribute(ExifInterface.TAG_SUBJECT_DISTANCE_RANGE, null);
      exifInterface.setAttribute(ExifInterface.TAG_SUBJECT_LOCATION, null);
      exifInterface.setAttribute(ExifInterface.TAG_USER_COMMENT, null);
      exifInterface.setAttribute(ExifInterface.TAG_WHITE_BALANCE, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_ALTITUDE, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_ALTITUDE_REF, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_AREA_INFORMATION, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_DOP, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_DATESTAMP, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_DEST_BEARING, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_DEST_BEARING_REF, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_DEST_DISTANCE, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_DEST_DISTANCE_REF, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_DEST_LATITUDE, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_DEST_LATITUDE_REF, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_DEST_LONGITUDE, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_DEST_LONGITUDE_REF, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_DIFFERENTIAL, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_IMG_DIRECTION, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_IMG_DIRECTION_REF, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_LATITUDE, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_LONGITUDE, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_MAP_DATUM, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_MEASURE_MODE, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_PROCESSING_METHOD, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_SATELLITES, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_SPEED, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_SPEED_REF, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_STATUS, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_TIMESTAMP, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_TRACK, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_TRACK_REF, null);
      exifInterface.setAttribute(ExifInterface.TAG_GPS_VERSION_ID, null);
      exifInterface.setAttribute(ExifInterface.TAG_INTEROPERABILITY_INDEX, null);
      exifInterface.setAttribute(ExifInterface.TAG_THUMBNAIL_IMAGE_LENGTH, null);
      exifInterface.setAttribute(ExifInterface.TAG_THUMBNAIL_IMAGE_WIDTH, null);
      exifInterface.saveAttributes();
      return  FileProvider
          .getUriForFile(HiddenActivity.this,
              getPackageName() + ".fileprovider", destinationFile);
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    }
  }

  private void copyFile(File sourceFile, File destinationFile) throws IOException {
    if (!sourceFile.exists()) {
      return;
    }
    FileChannel source = null;
    FileChannel destination = null;
    source = new FileInputStream(sourceFile).getChannel();
    destination = new FileOutputStream(destinationFile).getChannel();
    if (source != null) {
      destination.transferFrom(source, 0, source.size());
    }
    if (source != null) {
      source.close();
    }
    destination.close();
  }

  private Integer getArgSource() {
    return getIntent().getExtras().getInt(ARG_SOURCE);
  }

}