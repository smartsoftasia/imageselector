package com.smartsoftasia.library.imageselector;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import java.util.List;

import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by ssa-dev-4 on 16/5/2560.
 */
public class RxImagePicker {
  private static RxImagePicker instance;

  public static synchronized RxImagePicker with(Context context) {
    if (instance == null) {
      instance = new RxImagePicker(context.getApplicationContext());
    }
    return instance;
  }

  private Context context;
  private PublishSubject<Uri> publishSubject;
  private PublishSubject<List<Uri>> publishSubjectMultipleImages;

  private RxImagePicker(Context context) {
    this.context = context;
  }

  public Observable<Uri> getActiveSubscription() {
    return publishSubject;
  }

  public Observable<Uri> requestCamera() {
    publishSubject = PublishSubject.create();
    startCameraActivity();
    return publishSubject;
  }

  public Observable<List<Uri>> requestGallery() {
    publishSubjectMultipleImages = PublishSubject.create();
    startGalleryActivity();
    return publishSubjectMultipleImages;
  }

  void onImagePicked(Uri uri) {
    if (publishSubject != null) {
      publishSubject.onNext(uri);
      publishSubject.onCompleted();
    }
  }

  void onImagesPicked(List<Uri> uris) {
    if (publishSubjectMultipleImages != null) {
      publishSubjectMultipleImages.onNext(uris);
      publishSubjectMultipleImages.onCompleted();
    }
  }

  private void startCameraActivity() {
    Intent intent = new Intent(context, HiddenActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    intent.putExtra(HiddenActivity.ARG_SOURCE, HiddenActivity.SOURCE_CAMERA);
    context.startActivity(intent);
  }

  private void startGalleryActivity() {
    Intent intent = new Intent(context, HiddenActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    intent.putExtra(HiddenActivity.ARG_SOURCE, HiddenActivity.SOURCE_GALLERY);
    context.startActivity(intent);
  }
}